# frozen_string_literal: true

RSpec.describe RandomQuoteRss do
  it 'has a version number' do
    expect(RandomQuoteRss::VERSION).not_to be nil
  end

  it 'get quote via ruby URI' do
    expect(described_class.verify(false)).to eq(true)
  end

  it 'get quote via curl' do
    expect(described_class.verify(true)).to eq(true)
  end
end
