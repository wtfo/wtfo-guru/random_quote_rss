# frozen_string_literal: true

require 'random_quote_rss/quote'

#
# @module RandomQuoteRss
#
module RandomQuoteRss
  class Error < StandardError; end
  # Your code goes here...
  def self.verify(curl = false)
    quoter = Quote.new(curl)
    res = quoter.quote
    return true if res =~ %r{[a-zA-Z]+.* - [a-zA-Z]+.*}

    false
  end
end
